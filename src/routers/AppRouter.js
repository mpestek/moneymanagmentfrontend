import React from "react";
import { BrowserRouter, Route, Switch, Link, NavLink, Router } from 'react-router-dom';

import LoginPage from '../components/LoginPage';
import DashboardPage from '../components/DashboardPage';
import RegisterPage from '../components/RegisterPage';
import NotFoundPage from '../components/NotFoundPage';

const AppRouter = () => (
    <BrowserRouter>
        <div>            
            <Switch>
                <Route path="/login" component={LoginPage} />
                <Route path="/dashboard" component={DashboardPage} />
                <Route path="/register" component={RegisterPage} />
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;