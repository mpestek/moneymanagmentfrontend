function render() {
	return (
		<input hidden id="pictureInput" type="file"
			onChange={this.onPictureChange}
		/>
		<button onClick={(e) => {
			e.preventDefault();
			document.querySelector('#pictureInput').click();
		}}>
			Choose picture
		</button>
		{this.state.dataURL && <img src={this.state.dataURL} className="velicinaSlike" />}
	);
}