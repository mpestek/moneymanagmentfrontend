export const addAccount = (account) => ({
	type: 'ADD_ACCOUNT',
	account: account
});

export const deleteAccount = (accountId) => ({
	type: 'DELETE_ACCOUNT',
	accountId: accountId
});

export const loadAccounts = (accounts) => ({
	type: 'LOAD_ACCOUNTS',
	accounts: accounts
});

export const updateAccount = (account) => ({
	type: 'UPDATE_ACCOUNT',
	account: account
});

export const updateSavingCardAction = (accountId, savingCard) => ({
		type: 'UPDATE_SAVING_CARD',
		accountId: accountId,
		savingCard: savingCard
});