export const changeUsername = (username) => ({
    type: 'CHANGE_USERNAME',
    username
});

export const setUserInfo = (userInfo) => ({
        type: 'SET_USER_INFO',
        userInfo: userInfo
});

export const resetUserInfo = () => ({
    type: 'RESET_USER_INFO'
});