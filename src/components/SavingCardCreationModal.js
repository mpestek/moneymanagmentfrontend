import React from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { loadAccounts } from '../actions/accounts';
import { getAllSavingAccounts } from '../services/SavingAccounts';
import { createSavingCard } from '../services/SavingCard';

class SavingCardCreationModal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			cardName: '',
			cardDescription: ''
		}
	}

	onChangeSavingCardName = (name) => {
		this.setState({ cardName: name });
	}

	onChangeSavingCardDescription = (description) => {
		this.setState({ cardDescription: description });
	}

	onCreateSavingCard = () => {
		createSavingCard({
			access_token: this.props.access_token,
			accountId: this.props.accountId,
			savingCard: {
				Name: this.state.cardName,
				Description: this.state.cardDescription
			}
		}).then(() => this.reloadAccounts());
		
		this.props.closeModal();
	}

	reloadAccounts() {
		getAllSavingAccounts(this.props.access_token)
			.then(res => res.json())
			.then(res => this.props.dispatch(loadAccounts(res)));
	}

	render() {
		return (
			<Modal 
				isOpen={this.props.isOpen}
				className="SavingCardCreationModal__Content"
				style={{
					overlay: {
						backgroundColor: 'rgba(0, 2, 8, 0.85)'
					}
				}}
			>
				<h3 className="SavingCardCreationModal__Header">New card</h3>
				<div className="SavingCardCreationModal__Form">
					<div className="SavingCardCreationModal__Input SavingCardCreationModal__Name">
						<label>Card name</label>
						<input
							type="text"
							value={this.state.cardName}
							onChange={(e) => this.onChangeSavingCardName(e.target.value)}
							className="SavingCardCreationModal__InputField"
						/>		
					</div>
					<div className="SavingCardCreationModal__Input SavingCardCreationModal__Description">
						<label>Description</label>
						<input
							type="text"
							value={this.state.cardDescription}
							onChange={(e) => this.onChangeSavingCardDescription(e.target.value)}
							className="SavingCardCreationModal__InputField"
						/>
					</div>
					<div className="SavingCardCreationModal__Confirmation">
						<button className="SavingCardCreationModal__Submit" onClick={this.onCreateSavingCard}>Create card</button>
						<button className="SavingCardCreationModal__Cancel" onClick={this.props.closeModal}>Close</button>
					</div>
				</div>
			</Modal>
		);
	}
}

const mapStateToProps = (state) => ({
	access_token: state.login.userInfo.access_token
});

export default connect(mapStateToProps)(SavingCardCreationModal);