import React from 'react';
import { deleteSavingAccount } from '../services/SavingAccounts';
import { deleteAccount } from '../actions/accounts';
import { connect } from 'react-redux';
import AccountRemovalModal from './AccountRemovalModal';

class SavingAccount extends React.Component { 
	state = {
		accountRemovalConfirmationModalisOpen: false
	}

	constructor(props) {
		super(props);
	}

	deleteSavingAccount = () => {
		deleteSavingAccount(this.props.accessToken, this.props.account.AccountID);
		this.props.dispatch(deleteAccount(this.props.account.AccountID))
	}

	toggleAccountRemovalConfirmationModal = () => {
		this.setState((prevState) => ({
			accountRemovalConfirmationModalisOpen: !prevState.accountRemovalConfirmationModalisOpen
		}))
	}

	onAccountOpen = () => {
		this.props.history.push(`${this.props.location.pathname}/accounts/${this.props.account.AccountID}`);
	}

	render() {
		return (
			<div className="SavingAccount">
				<AccountRemovalModal
					isOpen={this.state.accountRemovalConfirmationModalisOpen}
					deleteSavingAccount={this.deleteSavingAccount}
					closeModal={this.toggleAccountRemovalConfirmationModal}
				/>
				<div className="SavingAccount__Header">
					<h3 className="SavingAccount__Heading">{this.props.account.Name}</h3>
					<button className="SavingAccount__Remove" onClick={this.toggleAccountRemovalConfirmationModal}>X</button>
				</div>
				<p>Balance: <span className="SavingAccount__Amount">{this.props.account.Balance}</span></p>
				<p>Total funds: <span className="SavingAccount__Amount">{this.props.account.TotalMoney}</span></p>
				<div className="SavingAccount__Options">
					<button className="SavingAccount__OpenButton" onClick={this.onAccountOpen}>Open</button>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	login: state.login,
	accounts: state.accounts
});

export default connect(mapStateToProps)(SavingAccount);