import React from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { getAllSavingAccounts, createSavingAccount } from '../services/SavingAccounts';
import { loadAccounts } from '../actions/accounts';

class AccountCreationModal extends React.Component {
	state = {
		accountName: '',
		initialFunds: ''
	}

	constructor(props) {
		super(props);
	}

	onChangeAccountName = (accountName) => {
		this.setState({ accountName: accountName });
	}

	onChangeInitialFunds = (initialFunds) => {
		this.setState({ initialFunds: initialFunds });
	}

	createAccount = () => {
		createSavingAccount(this.props.accessToken, {
			Name: this.state.accountName,
			Balance: this.state.initialFunds
		}).then(() => {
			this.reloadAccounts();
		});
		
		this.props.closeModal();
	}

	reloadAccounts() {
		getAllSavingAccounts(this.props.accessToken)
			.then(res => res.json())
			.then(res => this.props.dispatch(loadAccounts(res)));
	}

	render() {
		return (
			<Modal
				isOpen={this.props.isOpen}
				className="AccountCreationModal__Content"
				style={{
					overlay: {
						backgroundColor: 'rgba(0, 2, 8, 0.85)'
					}
				}}
			>
				<h3 className="AccountCreationModal__Header">New Account</h3>
				<div className="AccountCreationModal__Form">
					<div className="AccountCreationModal__Input AccountCreationModal__AccountName">
						<label>Account name</label>
						<input
							type="text"
							value={this.state.accountName}
							onChange={(e) => this.onChangeAccountName(e.target.value)}
							className="AccountCreationModal__InputField"
						/>
					</div>
					<div className="AccountCreationModal__Input AccountCreationModal__InitialFunds">
						<label>Initial funds</label>
						<input
							type="number"
							value={this.state.initialFunds}
							onChange={(e) => this.onChangeInitialFunds(e.target.value)}
							className="AccountCreationModal__InputField"
						/>
					</div>
					<div className="AccountCreationModal__Confirmation">
						<button className="AccountCreationModal__Submit" onClick={this.createAccount}>Create account</button>
						<button className="AccountCreationModal__Cancel" onClick={this.props.closeModal}>Cancel</button>
					</div>
				</div>
			</Modal>
		);
	}
}

export default connect()(AccountCreationModal);