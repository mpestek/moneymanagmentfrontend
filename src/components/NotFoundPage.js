import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
    <div>
        <p>The page you are looking for does not exist.</p>
        <Link to='/login'>Return to login page.</Link>
    </div>
);

export default NotFoundPage;