import React from 'react';
import Modal from 'react-modal';

const SavingCardRemovalModal = (props) => (
	<Modal
		isOpen={props.isOpen}
		className="SavingCardRemovalModal__Content"
		style={{
			overlay: {
				backgroundColor: 'rgba(0, 2, 8, 0.85)'
			}
		}}
	>
		<h2 className="SavingCardRemovalModal__Header">Delete card?</h2>
		<div className="SavingCardRemovalModal__Confirmation">
			<button className="SavingCardRemovalModal__Delete" onClick={props.deleteSavingCard}>Confirm</button>
			<button className="SavingCardRemovalModal__Cancel" onClick={props.closeModal}>Cancel</button>
		</div>			
	</Modal>
);

export default SavingCardRemovalModal;