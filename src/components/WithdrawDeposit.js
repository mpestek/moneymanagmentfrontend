import React from 'react';

class WithdrawDeposit extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			withdrawDepositAmount: '',
			withdrawOrDeposit: undefined
		};
	}

	onClickWithdraw = () => {
		this.setState(({ withdrawOrDeposit }) => {
			if (withdrawOrDeposit === 'withdraw')
				return { withdrawOrDeposit: undefined };

			return { withdrawOrDeposit: 'withdraw' };
		});
	}

	onClickDeposit = () => {
		this.setState(({ withdrawOrDeposit }) => {
			if (withdrawOrDeposit === 'deposit')
				return { withdrawOrDeposit: undefined };

			return { withdrawOrDeposit: 'deposit' };
		});
	}

	onChangeWithdrawDepositAmount = (amount) => {
		this.setState({
			withdrawDepositAmount: amount
		});
	}

	render() {
		return (
			<div 
				className={this.state.withdrawOrDeposit ? 'WithdrawDeposit WithdrawDeposit--active' : 'WithdrawDeposit'}
			> 
				<div className="WithdrawDeposit__Actions">
					<button 
						className={this.state.withdrawOrDeposit === 'withdraw' ? 'WithdrawDeposit__Action WithdrawDeposit__Action--active' : 'WithdrawDeposit__Action'}
						onClick={this.onClickWithdraw}
					>
						Withdraw
					</button>
					<button 
						className={this.state.withdrawOrDeposit === 'deposit' ? 'WithdrawDeposit__Action WithdrawDeposit__Action--active' : 'WithdrawDeposit__Action'}
						onClick={this.onClickDeposit}
					>
						Deposit
					</button>
				</div>
				{this.state.withdrawOrDeposit && 
					<form
						className="WithdrawDeposit__Form"
						onSubmit={(e) => this.props.onSubmitWithdrawDeposit(e, this.state.withdrawDepositAmount, this.state.withdrawOrDeposit)}
					>
						<input
							className="WithdrawDeposit__Amount"
							type="number" 
							value={this.state.withdrawDepositAmount}
							onChange={(e) => this.onChangeWithdrawDepositAmount(e.target.value)} />
						<button className="WithdrawDeposit__Confirm" type="submit">Confirm</button>
					</form>
				}
			</div>
		);
	}
}

export default WithdrawDeposit;