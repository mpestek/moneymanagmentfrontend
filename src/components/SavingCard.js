import React from 'react';
import SavingCardRemovalModal from '../components/SavingCardRemovalModal';
import { deleteSavingCard } from '../services/SavingCard';
import { getAllSavingAccounts } from '../services/SavingAccounts';
import { loadAccounts } from '../actions/accounts';
import { connect } from 'react-redux';
import WithdrawDeposit from '../components/WithdrawDeposit';
import { updateSavingCard } from '../services/SavingCard';
import { updateSavingCardAction } from '../actions/accounts';

class SavingCard extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			savingCardRemovalModalIsOpen: false
		};
	}

	toggleSavingCardRemovalModal = () => {
		this.setState((prevState) => ({
			savingCardRemovalModalIsOpen: !prevState.savingCardRemovalModalIsOpen
		}))
	}

	reloadAccounts() {
		getAllSavingAccounts(this.props.access_token)
			.then(res => res.json())
			.then(res => this.props.dispatch(loadAccounts(res)));
	}

	handleDeleteSavingCard = () => {
		deleteSavingCard({
			access_token: this.props.access_token,
			accountId: this.props.accountId,
			savingCardId: this.props.SavingCardID
		}).then(res => {
			if (res.status == 204)
				this.reloadAccounts();		
			}
		);

		this.toggleSavingCardRemovalModal();
	}

	onSubmitWithdrawDeposit = (e, amount, operation) => {
		e.preventDefault();
		
		let updatedSavingCard = {
			SavingCardID: this.props.SavingCardID,
			Name: this.props.Name,
			Description: this.props.Description,
			Balance: this.props.Balance,
			AccountID: this.props.AccountID
		};
		console.log(operation);
		if (operation === 'withdraw')
			updatedSavingCard.Balance = Number.parseFloat(updatedSavingCard.Balance) - Number.parseFloat(amount);
		else if (operation === 'deposit')
			updatedSavingCard.Balance = Number.parseFloat(updatedSavingCard.Balance) + Number.parseFloat(amount);
		else
			throw new Error();
		
		updateSavingCard({
			access_token: this.props.access_token,
			accountId: this.props.accountId,
			savingCard: updatedSavingCard
		})
		.then(res => {
			if (res.status == 204)
				this.reloadAccounts();
		});
	}

	updateSavingCardAndRereshUI = () => {
		updateSavingCard(this.props.access_token, this.props.accountId, updatedSavingCard)
			.then(res => {
				if (res.status == 204)
					this.props.dispatch(updateSavingCardAction(this.props.accountId, updatedSavingCard));
			});
	}

	render() {
		const {Name, Description, Balance} = this.props;

		return (
			<div className="SavingCard">
				<SavingCardRemovalModal
					isOpen={this.state.savingCardRemovalModalIsOpen}
					closeModal={this.toggleSavingCardRemovalModal}
					deleteSavingCard={this.handleDeleteSavingCard}
				/>
				<div className="SavingCard__Header">
					<div className="SavingCard__NameDescription">
						<h2>{Name}</h2>
						<p>{Description}</p>
					</div>
					<button className="SavingCard__Remove" onClick={this.toggleSavingCardRemovalModal}>X</button>
				</div>
				<div className="SavingCard__Body">
					<p className="SavingCard__Balance">Balance: {Balance}</p>
					<WithdrawDeposit 
						onSubmitWithdrawDeposit={this.onSubmitWithdrawDeposit}
					/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	access_token: state.login.userInfo.access_token
});

export default connect(mapStateToProps)(SavingCard);