import React from 'react';
import { connect } from 'react-redux';
import { getAllSavingAccounts } from '../services/SavingAccounts';
import SavingAccount from './SavingAccount';
import { loadAccounts } from '../actions/accounts';
import AccountCreationModal from './AccountCreationModal';

class AccountPanel extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			username: this.props.login.userInfo.userName,
			accessToken: this.props.login.userInfo.access_token,
			accountCreationModalIsOpen: false
		};
	}
	
	componentDidMount() {
		getAllSavingAccounts(this.state.accessToken)
			.then(res => res.json())
			.then(res => this.props.dispatch(loadAccounts(res)));
	}

	toggleAccountCreationModal = () => {
		this.setState((prevState) => ({
			accountCreationModalIsOpen: !prevState.accountCreationModalIsOpen
		}));
	}

	render() {
		return (
			<div className="AccountPanel">
				<AccountCreationModal
					isOpen={this.state.accountCreationModalIsOpen}
					closeModal={this.toggleAccountCreationModal}
					username={this.state.username}
					accessToken={this.state.accessToken}
				/>
				<div className="AccountPanel__Header">
					<h2 className="AccountPanel__Heading">Accounts</h2>
					<button className="AccountPanel__NewAccount" onClick={this.toggleAccountCreationModal}>New account</button>
				</div>
				<div className="AccountPanel__AccountContainer">
					{this.props.accounts.map(
						account => 
							<SavingAccount 
								key={account.AccountID}
								account={account} 
								accessToken={this.state.accessToken}
								history={this.props.history}
								location={this.props.location}
							/>
					)}
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	login: state.login,
	accounts: state.accounts.accounts
});

export default connect(mapStateToProps)(AccountPanel);