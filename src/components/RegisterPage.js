import React from 'react';
import { Link } from 'react-router-dom';
import config from '../../config/config';
import isEmail from 'validator/lib/isEmail';

class RegisterPage extends React.Component {
	state = {
		email: '',
		password: '',
		confirmPassword: '',
		error: '',
		status: ''
	}

	constructor(props) {
		super(props);
	}

	onEmailChange = (email) => {
		this.setState({ email });

		if (isEmail(email))
			this.setState({ error: '' });
		else
			this.setState({ error: 'Email is not in a valid format!'});

	}

	onPasswordChange = (password) => {
		this.setState({ password });

		this.checkIfPasswordsMatchOnPasswordChange(password);
	}

	checkIfPasswordsMatchOnPasswordChange = (password) => {
		if (this.state.confirmPassword && this.state.confirmPassword !== password)
			this.setState({ error: 'Passwords must match!' });
		else if (this.state.confirmPassword === password)
			this.setState({ error: '' });
	}

	onConfirmPasswordChange = (confirmPassword) => {
		this.setState({ confirmPassword });

		this.checkIfPasswordsMatchOnConfirmPasswordChange(confirmPassword);
	}

	checkIfPasswordsMatchOnConfirmPasswordChange = (confirmPassword) => {
		if (confirmPassword !== this.state.password)
			this.setState({ error: 'Passwords must match!' });
		else if (confirmPassword === this.state.password)
			this.setState({ error: '' });
	}

	onRegister = (e) => {
		e.preventDefault();

		this.attemptRegister()
			.then(res => {
				if (res.ok)
				{
					this.setState({ 
						error: '',
						status:'Your account has been successfully registered!'
					});
					this.clearRegistrationFields();
				}
				else
					this.setState({ error: 'Attempt to register failed.'});
			})
	}

	attemptRegister = () => {
		let req = new Request(config.apiUrl + 'api/account/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				'Email': this.state.email,
				'Password': this.state.password,
				'ConfirmPassword': this.state.confirmPassword
			})
		});

		return fetch(req);
	}

	clearRegistrationFields = () => {
		this.setState({ 
			email: '',
			password: '',
			confirmPassword: ''
		});
	}

	render() {
		return (
			<div className="RegisterPage">
				<div className="RegisterPage__RegisterFormContainer">
					<h2 className="RegisterForm__Header">Register</h2>
					<form
						onSubmit={this.onRegister}
						className="RegisterPage__RegisterForm"
					>
						<div className="RegisterForm__Input RegisterForm__Email">
							<label>Email</label>
							<input 
								type="text"
								value={this.state.email}
								onChange={(e) => this.onEmailChange(e.target.value)}
								className="RegisterForm__Credentials"
							/>
						</div>
						<div className="RegisterForm__Input RegisterForm__Password">
							<label>Password</label>
							<input 
								type="password"
								value={this.state.password}
								onChange={(e) => this.onPasswordChange(e.target.value)}
								className="RegisterForm__Credentials"
							/>
						</div>
						<div className="RegisterForm__Input RegisterForm__ConfirmPassword">
							<label>Confirm password</label>
							<input 
								type="password"
								value={this.state.confirmPassword}
								onChange={(e) => this.onConfirmPasswordChange(e.target.value)}
								className="RegisterForm__Credentials"
							/>
						</div>
						{this.state.error && <p>{this.state.error}</p>}
						{this.state.status && <p>{this.state.status}</p>}
						<div className="RegisterForm__Input">
							<button className="RegisterForm__Submit" disabled={this.state.error}>Register</button>
						</div>
						<p>Or go back to the <Link to='/login'>Login</Link> page if you already have an account!</p>
					</form>
				</div>
			</div>
		);
	}
}

export default RegisterPage;