import React from 'react';
import Modal from 'react-modal';

const AccountRemovalModal = (props) => (
	<Modal
		isOpen={props.isOpen}
		className="AccountRemovalModal__Content"
		style={{
			overlay: {
				backgroundColor: 'rgba(0, 2, 8, 0.85)'
			}
		}}
	>
		<h2 className="AccountRemovalModal__Header">Delete account?</h2>
		<div className="AccountRemovalModal__Confirmation">
			<button className="AccountRemovalModal__Delete" onClick={props.deleteSavingAccount}>Delete</button>
			<button className="AccountRemovalModal__Cancel" onClick={props.closeModal}>Cancel</button>
		</div>
	</Modal>
);

export default AccountRemovalModal;