import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { setUserInfo } from '../actions/login';
import { attemptLogin } from '../services/Login';

class LoginPage extends React.Component {
	state = {
		username: 'harispohara@gmail.com',
		password: 'Haris1238924!',
		error: '',
		dataURL: ''
	}

	constructor(props) {
		super(props);
	}

	onPictureChange = (e) => {
		e.preventDefault();

		this.setState({ dataURL: URL.createObjectURL(e.target.files[0]) });
	}

	componentDidUpdate() {
		console.log(this.state.dataURL);
	}

	onUsernameChange = (username) => {
		this.setState({ username });
	}

	onPasswordChange = (password) => {
		this.setState({ password });
	}

	onLogin = (e) => {
		e.preventDefault();
		
		this.login();
	}

	login = () => {
		attemptLogin(this.state.username, this.state.password)
			.then(res => res.json())
			.then(res => {
				if (res.access_token)
				{
					this.setState({ error: '' });
					this.props.dispatch(setUserInfo(res))
					this.props.history.push('/dashboard');
				}
				else
					this.setState({ error: 'Provided credentials are not valid!' });
			})
	}

	render() {
		return (
			<div className="LoginPage">
				<div className="LoginPage__LoginFormContainer">
					<h2 className="LoginForm__Header">Member login</h2>
					<form 
						className="LoginPage__LoginForm"
						onSubmit={this.onLogin}
					>
						<div className="LoginForm__Input LoginForm__Email">
							<label>Email</label>
							<input 
								type="text" 
								onChange={(e) => this.onUsernameChange(e.target.value)}
								value={this.state.username}
								className="LoginForm__Credentials "
							/>
						</div>
						<div className="LoginForm__Input LoginForm__Password">
							<label>Password</label>
							<input
								type="password" 
								onChange={(e) => this.onPasswordChange(e.target.value)} 
								value={this.state.password}
								className="LoginForm__Credentials"
							/>
						</div>
						<div className="LoginForm__Input">
							<button className="LoginForm__Submit">Login</button>
						</div>
						<div className="LoginForm__Error">
							{this.state.error && <p>{this.state.error}</p>}
						</div>
						<div className="LoginForm__Register">
							<p>Or <Link to="/register">Register</Link> if you don't have an account yet. </p>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	login: state.login
});

export default connect(mapStateToProps)(LoginPage);

