import React from 'react';
import SavingCard from '../components/SavingCard';
import SavingCardCreationModal from '../components/SavingCardCreationModal';
import { connect } from 'react-redux';
import { updateAccount } from '../actions/accounts';
import { updateSavingAccount } from '../services/SavingAccounts';
import WithdrawDeposit from '../components/WithdrawDeposit';

class AccountDashboard extends React.Component {
	constructor(props) {
		super(props);

		this.accountId = this.props.match.params.id;
		this.account = this.getOpenedAccount();
		
		this.state = {
			savingCardCreationModalIsOpen: false
		};
	}

	getOpenedAccount = () => this.props.accounts.find(account => account.AccountID == this.accountId);

	toggleSavingCardCreationModal = () => {
		this.setState((prevState) => ({
			savingCardCreationModalIsOpen: !prevState.savingCardCreationModalIsOpen
		}));
	}

	getSavingCardsForAccount = () => {
		return this.getOpenedAccount().SavingCards;
	}

	deleteSavingCard = (savingCardId) => {
		console.log(savingCardId);
		this.toggleSavingCardRemovalModal();
	}

	onSubmitWithdrawDeposit = (e, amount, operation) => {
		e.preventDefault();
		
		let updatedAccount = {...this.getOpenedAccount()};

		if (operation === 'withdraw') {
			updatedAccount.Balance = Number.parseFloat(updatedAccount.Balance) - Number.parseFloat(amount);
			updatedAccount.TotalMoney = Number.parseFloat(updatedAccount.TotalMoney) - Number.parseFloat(amount);
		}
		else if (operation === 'deposit') {
			updatedAccount.Balance = Number.parseFloat(updatedAccount.Balance) + Number.parseFloat(amount);
			updatedAccount.TotalMoney = Number.parseFloat(updatedAccount.TotalMoney) + Number.parseFloat(amount);
		}
		else
			throw new Error();
		
		updateSavingAccount(this.props.access_token, updatedAccount)
			.then(res => {
				if (res.status == 204) {
					console.log(res);
					this.props.dispatch(updateAccount(updatedAccount));
				}
				else
					console.log(res);
			});
	}

	render() {
		const accountSavingCards = this.getSavingCardsForAccount()
																	.map(savingCard => (
																		<SavingCard 
																			key={savingCard.SavingCardID} 
																			toggleSavingCardRemovalModal={this.toggleSavingCardRemovalModal}
																			accountId={this.accountId}
																			{...savingCard} />
																	));

		return (
			<div className="AccountDashboard">
				<div className="AccountDashboard__Header">
					<div className="AccountDashboard__AccountStats">
						<h1>Account: {this.getOpenedAccount().Name}</h1>
						<h3>Balance: {this.getOpenedAccount().Balance}</h3>
						<h3>Total funds: {this.getOpenedAccount().TotalMoney}</h3>
					</div>
					<WithdrawDeposit
						onSubmitWithdrawDeposit={this.onSubmitWithdrawDeposit}
					/>
				</div>
				<SavingCardCreationModal 
					isOpen={this.state.savingCardCreationModalIsOpen}
					closeModal={this.toggleSavingCardCreationModal}
					accountId={this.accountId}
				/>
				<div className="AccountDashboard__SavingCards">
					<div className="AccountDashboard__SavingCardsHeader">
						<h2 className="AccountDashboard__SavingCardsHeading">Saving cards</h2>
						<button 
							className="AccountDashboard__SavingCardsNew" 
							onClick={this.toggleSavingCardCreationModal}
						>
							New card
						</button>
					</div>
					<div className="AccountDashboard_SavingCardsContainer">
						{accountSavingCards}
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	accounts: state.accounts.accounts,
	access_token: state.login.userInfo.access_token
});

export default connect(mapStateToProps)(AccountDashboard);