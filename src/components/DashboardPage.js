import React from 'react';
import { connect } from 'react-redux';
import { resetUserInfo } from '../actions/login';
import AccountPanel from './AccountPanel';
import { Route, Switch } from 'react-router-dom';
import AccountDashboard from './AccountDashboard';

class DashboardPage extends React.Component {
	constructor(props) {
		super(props);

		if (!props.login.userInfo)
			props.history.push('/login');

		console.log(this.props);
	}
	
	onLogout = () => {
		this.props.history.push('/login');
		this.props.dispatch(resetUserInfo());
	}

	render() {
		if (!this.props.login.userInfo)
			return (
				<div>
					<p>Please log in to view this page.</p>
				</div>
			);
		return (
			<div className="DashboardPage">
				<div className="DashboardPage__Header">
					<h1 className="DashboardPage__Heading">Welcome, {this.props.login.userInfo.userName}.</h1>
					<button className="DashboardPage__Logout" onClick={this.onLogout}>Logout</button>
				</div>
				<div className="DashboardPage__DashboardContainer">
					<Switch>
						<Route exact path={`${this.props.match.path}`} component={AccountPanel} />
						<Route path={`${this.props.match.path}/accounts/:id`} component={AccountDashboard} />
					</Switch>
				</div>
			</div>
		);
	}
};

const mapStateToProps = (state) => ({
	login: state.login
});

export default connect(mapStateToProps)(DashboardPage);