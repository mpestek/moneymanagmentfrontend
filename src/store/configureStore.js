import { createStore, combineReducers } from 'redux';
import { loginReducer } from '../reducers/login';
import { accountsReducer } from '../reducers/accounts';

export default () => {
    const store = createStore(
        combineReducers({
            login: loginReducer,
            accounts: accountsReducer
        }),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    return store;
};