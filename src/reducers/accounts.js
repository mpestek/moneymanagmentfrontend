const defaultAccountsReducerState = {
	accounts: []
};

const accountsReducer = (state = defaultAccountsReducerState, action) => {
	switch (action.type) {
		case 'LOAD_ACCOUNTS':
			return {
				...state,
				accounts: action.accounts
			}
		case 'ADD_ACCOUNT':
			return {
				...state,
				accounts: [...accounts, action.account]
			};
		case 'DELETE_ACCOUNT':
			return {
				...state,
				accounts: state.accounts.filter(x => x.AccountID != action.accountId)
			};
		case 'UPDATE_ACCOUNT':
			return {
				...state,
				accounts: state.accounts.map(x => {
					if (x.AccountID == action.account.AccountID)
						return action.account;
					return x;
				})
			};
		case 'UPDATE_SAVING_CARD':
			let accountsWithUpdatedSavingCard = 
				state.accounts
					.map(account => {
						if (account.AccountID != action.accountId)
							return account;
						
						account.SavingCards = 
							account.SavingCards
								.map(card => {
									if (card.SavingCardID != action.savingCard.SavingCardID)
										return card;
									
									return action.savingCard;
								});
						return account;
					});
					
			return {
				...state,
				accounts: accountsWithUpdatedSavingCard
			};
		default:
			return state;
	}
};

export { accountsReducer };