const defaultLoginReducerState = {
    userInfo: undefined
};

const loginReducer = (state = defaultLoginReducerState, action) => {
    switch (action.type) {
        case 'SET_USER_INFO':
            return {
                ...state,
                userInfo: action.userInfo
            }
        case 'RESET_USER_INFO':
            return {
                ...state, 
                userInfo: undefined
            }
        default:
            return state;
    }
};

export { loginReducer };