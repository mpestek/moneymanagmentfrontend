import config from '../../config/config';

export const createSavingCard = ({access_token, accountId, savingCard}) => {
	let req = new Request(config.apiUrl + 'savingaccounts/' + accountId + '/savingcards', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + access_token
		},
		body: JSON.stringify(savingCard)
	});

	return fetch(req);
}

export const deleteSavingCard = ({access_token, accountId, savingCardId}) => {
	let req = new Request(config.apiUrl + 'savingaccounts/' + accountId + '/savingcards/' + savingCardId, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + access_token
		}
	});

	return fetch(req);
}

export const updateSavingCard = ({access_token, accountId, savingCard}) => {
	let req = new Request(config.apiUrl + 'savingaccounts/' + accountId + '/savingcards/' + savingCard.SavingCardID, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + access_token
		},
		body: JSON.stringify(savingCard)
	});

	return fetch(req);
}