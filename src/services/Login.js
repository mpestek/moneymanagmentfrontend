import config from '../../config/config';

export const attemptLogin = (username, password) => {
	const req = new Request(config.apiUrl + 'Token', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: new URLSearchParams({
			'grant_type': 'password',
			'username': username,
			'password': password
		})
	});

	return fetch(req);
}