import config from '../../config/config';

export const getAllSavingAccounts = (accessToken) => {
	let req = new Request(config.apiUrl + 'savingaccounts/', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + accessToken
		}
	});

	return fetch(req);
}

export const deleteSavingAccount = (accessToken, accountId) => {
	let req = new Request(config.apiUrl + 'savingaccounts/' + accountId, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + accessToken
		}
	});

	return fetch(req);
}

export const createSavingAccount = (accessToken, account) => {
	let req = new Request(config.apiUrl + 'savingaccounts/', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + accessToken
		},
		body: JSON.stringify(account)
	});

	return fetch(req);
}

export const updateSavingAccount = (accessToken, account) => {
	console.log(account);
	let req = new Request(config.apiUrl + 'savingaccounts/', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + accessToken
		},
		body: JSON.stringify(account)
	});

	return fetch(req);
}